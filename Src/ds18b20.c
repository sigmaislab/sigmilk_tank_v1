/*
 * ds18b20.c
 *
 *  Created on: 1 de ago de 2019
 *      Author: oscar
 */


#include <ds18b20.h>


GPIO_InitTypeDef GPIO_InitStruct = {0};


/*************CHANGE PIN STATE FUNCTIONS*****************************************/
void gpio_set_input (void)
{

  GPIO_InitStruct.Pin = TEMP_EXT_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(TEMP_EXT_GPIO_Port, &GPIO_InitStruct);
}

void gpio_set_output (void)
{

  GPIO_InitStruct.Pin = TEMP_EXT_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  HAL_GPIO_Init(TEMP_EXT_GPIO_Port, &GPIO_InitStruct);
}

void gpio_set_new_input (void)
{

  GPIO_InitStruct.Pin = TEMP_EXT_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init_Input(TEMP_EXT_GPIO_Port, &GPIO_InitStruct);
}

void gpio_set_new_output (void)
{

  GPIO_InitStruct.Pin = TEMP_EXT_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init_Output(TEMP_EXT_GPIO_Port, &GPIO_InitStruct);
}

/*******************DS18B20 FUNCTIONS*****************************************/
void ds18b20_reset (void)
{
    gpio_set_output();

    for (int var = 0; var < 120; var++) //54 for 2.097MHz;
    {
        DS18B20_RESET
    }

    gpio_set_input ();    // set the pin as input

    for (int var = 0; var < 240; var++)//54 for 2.097MHz;
    {
        DS18B20_SET
    }
    gpio_set_output();// Initialize bit counter

}

void ds18b20_write (uint8_t data)
{
    uint8_t bit;

    for (bit = 0; bit < 8; bit++)               // Loop 8 bits to write a byte
    {

        if((data >> bit) & 0x01)  // if the bit is high
        {
            DS18B20_RESET
            DS18B20_SET
            for (int var = 0; var < 8; var++)
            {
                DS18B20_SET
            }

        }
        else  // if the bit is low
        {
            // write 0
            //DS18B20_RESET
            for (int var = 0; var < 20; var++) //9 for 2.097MHz;
                 {
                    DS18B20_RESET
                 }
            DS18B20_SET
         //   gpio_set_input ();
        }
    }
}

uint8_t ds18b20_read (void)
{
    uint8_t bit;                                // Initialize bit counter
    uint8_t Data = 0;                           // Initialize return data byte
    //gpio_set_input();
    for (bit = 0; bit < 8; bit++)               // Loop 8 bits to read a byte
    {
        //DS18B20_SET
        DS18B20_RESET
        gpio_set_new_input ();  // set as input
        if(HAL_GPIO_ReadPin(TEMP_EXT_GPIO_Port,TEMP_EXT_Pin)){
            Data |= (0x01 << bit);  // If high then read 1, else 0
        }
        for (int var = 0; var < 7; var++)
        {
            DS18B20_SET
        }

        gpio_set_new_output ();   // set as output

    }
    return Data;                                // Return byte
}

void fn_get_ds18b20(void)
{


    ds18b20_reset();
    ds18b20_write(SKIP_ROM);
    ds18b20_write(CONV_T);
    HAL_Delay(1000);
    ds18b20_reset();
    ds18b20_write(SKIP_ROM);
    ds18b20_write(READ_SCR);
    for (byte = 0; byte < 9; byte++){
        scratch[byte] = ds18b20_read();     // Read full identification number
        convertido++;
    }
    HAL_Delay(100);
    convertido = scratch[1];
        convertido = (convertido << 8) | scratch[0];
        //negative temperature
        //0.....126 = negative (127 positions)
        if (scratch[1] & 0xf0 ){
        	temp_negative = true;
            convertido = ~convertido + 1;
            temp_ds18b20 = convertido *6.25;
            temp_ds18b20/=100;
            temp_ds18b20*=(-1);
        }
        //positive temperature
        //128...255 = positive (127 positions)
        //127 = 0 (1 position)
        else{
                //temperatura = convertido * (unsigned long)0.0625*100;
        	temp_negative= false;
            temp_ds18b20 = convertido *6.25;
            temp_ds18b20/=100;
            }
}

void fn_ds18b20_init()
{
/*    uint8_t identification[8]={0}; // = {0x00};           // 64-bit identification number
    ds18b20_reset();
    ds18b20_write(READ_ROM);
    for (byte = 0; byte < 8; byte++)
    {
        identification[byte] = ds18b20_read();     // Read full identification number
    }
    convertido = 0;*/
}
