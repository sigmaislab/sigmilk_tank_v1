/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2019 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
/*
#include <math.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "lsm303agr_reg.h"
#include <sigfox.h>
#include <sensors.h>
*/

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
/*************************************************************************************************/
/*    ENUMS                                                                                      */
/*************************************************************************************************/
typedef enum
{
    NONE,
    INITIALIZING,
    ERRO,
    TEST,
    COUNTER,
    SEND_UPLINK,
	SEND_DOWNLINK,
    MEASURE,
	SEND_ALERT,
} Machine_States_t;




/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
//------------------------------------------------------------------------------
// Single-wire commands Registers
//------------------------------------------------------------------------------
#define SecondsToCommunicate    900          //5H = 18000 1H = 3600s //30MIN = 1800s //15min = 900s // 10min = 600s //5min = 300s //1min = 60s
#define Firmware_Version        1.4           // downlink
#define secondsOneDay           86400 //seconds in a day

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

__IO ITStatus UartReady = RESET;
ADC_ChannelConfTypeDef sConfig = {0};
/* Prescaler declaration */
uint32_t uwPrescalerValue = 0;
/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc;
DMA_HandleTypeDef hdma_adc;

I2C_HandleTypeDef hi2c1;
I2C_HandleTypeDef hi2c2;
I2C_HandleTypeDef hi2c3;

TIM_HandleTypeDef htim3;

UART_HandleTypeDef huart2;
UART_HandleTypeDef huart4;
UART_HandleTypeDef huart5;

/* USER CODE BEGIN PV */
/* ///////////////////////////// DEFINES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/


//**************GLOBAL VARIABLES*********************************
Machine_States_t e_Current_Machine_State = INITIALIZING,e_Previous_Machine_State = NONE;
uint32_t adc[4]={0},buffer_adc[4]={0};

//unsigned int previous_count_time=1;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_ADC_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_USART5_UART_Init(void);
static void MX_I2C1_Init(void);
static void MX_I2C2_Init(void);
static void MX_USART4_UART_Init(void);
static void MX_I2C3_Init(void);
static void MX_TIM3_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */


/*******************ADC FUNCTIONS*****************************************/
void fn_get_adc_channels()
{
    /* ### - 4 - Start conversion in DMA mode ################################# */
     if (HAL_ADC_Start_DMA(&hadc,(uint32_t*)buffer_adc,4) != HAL_OK)
     {
       Error_Handler();
     }

    HAL_Delay(100);
    HAL_ADC_Stop_DMA(&hadc);
    HAL_Delay(100);
}

//*********************** CONTROL FUNCTIONS ***************************
void fn_Change_Machine_State(Machine_States_t state)
{
    e_Previous_Machine_State = e_Current_Machine_State;
    e_Current_Machine_State = state;
}


///////////////////////END FUNCTIONS///////////////////////////////////////////

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)

{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_ADC_Init();
  MX_USART2_UART_Init();
  MX_USART5_UART_Init();
  MX_I2C1_Init();
  MX_I2C2_Init();
  MX_USART4_UART_Init();
  MX_I2C3_Init();
  MX_TIM3_Init();
  /* USER CODE BEGIN 2 */
  blink();

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
      while (1)
      {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */

          switch (e_Current_Machine_State)
                  {
                  case NONE:
                      break;
                  case INITIALIZING:
                	  fn_init();

                      if(st_data_sensor_e.bat_value<30)
                    	  fn_Change_Machine_State(ERRO);
                      else
                      {
                    	  fn_Change_Machine_State(COUNTER);
                      }
                      break;
                  case ERRO:
                	  blink();
                      fn_read_sensors();
                	  if(st_data_sensor_e.bat_value>30)
                		  fn_Change_Machine_State(COUNTER);
                      break;
                  case TEST:
                      break;
                  case COUNTER:
                	    if(tanq_open==true)
                	    {
                	        open_tank_timer++;
                	    }
                	    if(((st_timers.cont_time%st_config_time_t.temperature_sensor_reading==0 && st_config_time_t.temperature_sensor_reading !=0)
                	    		||	(st_timers.cont_time%st_config_time_t.ph_sensor_reading==0 && st_config_time_t.ph_sensor_reading !=0)
                	    		||	(st_timers.cont_time%st_config_time_t.volume_sensor_reading==0 && st_config_time_t.volume_sensor_reading!=0))
                	    		|| 	st_timers.cont_time == (st_timers.cont_time-60))
                	    {
                	    	fn_Change_Machine_State(MEASURE);
                	    }
                	    if(st_timers.cont_time%st_config_time_t.inside_wt==0)
                	    {
                	    	if(e_day_to_work == DAYONE)
                	    	{
								if((st_timers.cont_time>=(st_worktime.start_day_one*3600))
										&& st_timers.cont_time<=(st_worktime.end_day_one*3600))
								{
									fn_Change_Machine_State(SEND_UPLINK);
								}
                	    	}
							else if(e_day_to_work == DAYTWO)
							{
								if((st_timers.cont_time>=(st_worktime.start_day_two*3600))
										&& st_timers.cont_time<=(st_worktime.end_day_two*3600))
								{
									fn_Change_Machine_State(SEND_UPLINK);
								}
							}
							else if (e_day_to_work == DAYTHREE)
							{
								if((st_timers.cont_time>=(st_worktime.start_day_three*3600))
										&& st_timers.cont_time<=(st_worktime.end_day_three*3600))
								{
									fn_Change_Machine_State(SEND_UPLINK);
								}
							}
                	    }
                	    else if(st_timers.cont_time%st_config_time_t.outside_wt==0)
                	    {
                	    	fn_Change_Machine_State(SEND_UPLINK);
                	    }
                	    if(st_timers.seconds_today==secondsOneDay)
                	    	fn_Change_Machine_State(SEND_DOWNLINK);
                      break;
                  case SEND_UPLINK:
                	  st_timers.cont_time=0;
                      hea_value=0;
                      e_sensors_group = ALLSENSORS;
                      fn_read_sensors();
                      fn_send_report_frame();
                      if(st_data_sensor_e.bat_value<30)
                    	  fn_Change_Machine_State(ERRO);
                      else
                    	  fn_Change_Machine_State(COUNTER);
                      break;
                  case SEND_DOWNLINK:
                	  fn_send_daily_frame();
                	  if(config_flag == true)
                		  fn_send_config_frame();
                	  fn_Change_Machine_State(COUNTER);
                	  break;
                  case MEASURE:
                	  st_timers.Time_Machine++;
                	  st_flags.alert_cj_1 = false;
					  st_flags.alert_cj_2 = false;
					  st_flags.alert_cj_3 = false;
                	  //if all groups
                    if(st_config_time_t.temperature_sensor_reading!=0||
                       st_config_time_t.ph_sensor_reading!=0         ||
				   	   st_config_time_t.volume_sensor_reading!=0)
                	  {
							if(st_timers.cont_time%st_config_time_t.temperature_sensor_reading==0)
							{
								e_sensors_group = SENSORS_1;
							}
							if(st_timers.cont_time%st_config_time_t.ph_sensor_reading==0)
							{
								e_sensors_group = SENSORS_2;
							}
							if(st_timers.cont_time%st_config_time_t.volume_sensor_reading==0)
							{
								e_sensors_group = SENSORS_3;
							}
							if(st_timers.cont_time%st_config_time_t.temperature_sensor_reading==0 &&
							st_timers.cont_time%st_config_time_t.ph_sensor_reading==0)
							{
								e_sensors_group = SENSORS_1_2;
							}
							if(st_timers.cont_time%st_config_time_t.temperature_sensor_reading==0
							&& st_timers.cont_time%st_config_time_t.volume_sensor_reading==0 )
							{
								e_sensors_group = SENSORS_1_3;
							}
							if(st_timers.cont_time%st_config_time_t.ph_sensor_reading==0
							&& st_timers.cont_time%st_config_time_t.volume_sensor_reading==0)
							{
								e_sensors_group = SENSORS_3_2;
							}
                	  	  }
                    else
						{
							e_sensors_group = ALLSENSORS;
						}

                  	fn_read_sensors();
                  	if(e_sensors_group == ALLSENSORS || e_sensors_group == SENSORS_2 ||e_sensors_group == SENSORS_1_2||e_sensors_group == SENSORS_3_2)
                  	{
                  		if(st_config_trigger_t.threshold_ph!=0)
                  		{
							if(st_data_sensor_e.ph_value-st_data_sensor_previwes_e.ph_value>=st_config_trigger_t.threshold_ph
									||st_data_sensor_previwes_e.ph_value-st_data_sensor_e.ph_value>=st_config_trigger_t.threshold_ph)
							{
								st_flags.alert_cj_2 = true;
							}
                  		}
                  	}
                  	if(e_sensors_group == ALLSENSORS || e_sensors_group == SENSORS_3 ||e_sensors_group == SENSORS_1_3||e_sensors_group == SENSORS_3_2)
					{
							if((st_data_sensor_e.vol_value-st_data_sensor_previwes_e.vol_value>=st_config_trigger_t.threshold_volume
											&& st_config_trigger_t.threshold_volume!=0)
									|| (st_data_sensor_previwes_e.vol_value-st_data_sensor_e.vol_value>=st_config_trigger_t.threshold_volume
											&& st_config_trigger_t.threshold_volume!=0)
									|| (st_data_sensor_e.vol_value >= st_config_trigger_t.max_volume
											&& st_config_trigger_t.max_volume!=0)
									|| (st_data_sensor_e.vol_value <= st_config_trigger_t.min_volume
											&& st_config_trigger_t.min_volume!=0))
							{
								st_flags.alert_cj_3 = true;
							}
					}
                  	if(e_sensors_group == ALLSENSORS || e_sensors_group == SENSORS_1 ||e_sensors_group == SENSORS_1_2||e_sensors_group == SENSORS_1_3)
					{
						if((st_data_sensor_e.ds18b20_value-st_data_sensor_previwes_e.ds18b20_value>=st_config_trigger_t.threshold_temperature
										&& st_config_trigger_t.threshold_temperature!=0)
								|| (st_data_sensor_previwes_e.ds18b20_value-st_data_sensor_e.ds18b20_value>=st_config_trigger_t.threshold_temperature
										&& st_config_trigger_t.threshold_temperature!=0)
								|| (st_data_sensor_e.ds18b20_value >= st_config_trigger_t.max_temperature
										&& st_config_trigger_t.max_temperature!=0)
								|| (st_data_sensor_e.ds18b20_value <= st_config_trigger_t.min_temperature
										&& st_config_trigger_t.min_temperature!=0))
							{
								st_flags.alert_cj_1 = true;
							}
					}

                  	if(st_flags.alert_cj_1 == true || st_flags.alert_cj_2==true || st_flags.alert_cj_3 == true)
                  	{
                  		fn_Change_Machine_State(SEND_ALERT);
                  	}else
                  	{
                  		fn_Change_Machine_State(COUNTER);
                  	}
                  	break;
                  case SEND_ALERT:
                	  fn_send_alert_frame();
                	  fn_Change_Machine_State(COUNTER);
                	  break;
                  }
      }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /**Configure the main internal regulator output voltage 
  */
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /**Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_MSI;
  RCC_OscInitStruct.MSIState = RCC_MSI_ON;
  RCC_OscInitStruct.MSICalibrationValue = 0;
  RCC_OscInitStruct.MSIClockRange = RCC_MSIRANGE_5;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /**Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_MSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART2|RCC_PERIPHCLK_I2C1
                              |RCC_PERIPHCLK_I2C3;
  PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
  PeriphClkInit.I2c1ClockSelection = RCC_I2C1CLKSOURCE_PCLK1;
  PeriphClkInit.I2c3ClockSelection = RCC_I2C3CLKSOURCE_PCLK1;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
  //HAL_RCC_MCOConfig(RCC_MCO1, RCC_MCO1SOURCE_SYSCLK, RCC_MCODIV_1);
}

/**
  * @brief ADC Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC_Init(void)
{

  /* USER CODE BEGIN ADC_Init 0 */

  /* USER CODE END ADC_Init 0 */

  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC_Init 1 */

  /* USER CODE END ADC_Init 1 */
  /**Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion) 
  */
  hadc.Instance = ADC1;
  hadc.Init.OversamplingMode = DISABLE;
  hadc.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV4;
  hadc.Init.Resolution = ADC_RESOLUTION_12B;
  hadc.Init.SamplingTime = ADC_SAMPLETIME_39CYCLES_5;
  hadc.Init.ScanConvMode = ADC_SCAN_DIRECTION_FORWARD;
  hadc.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc.Init.ContinuousConvMode = ENABLE;
  hadc.Init.DiscontinuousConvMode = DISABLE;
  hadc.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc.Init.DMAContinuousRequests = ENABLE;
  hadc.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
  hadc.Init.Overrun = ADC_OVR_DATA_OVERWRITTEN;
  hadc.Init.LowPowerAutoWait = DISABLE;
  hadc.Init.LowPowerFrequencyMode = DISABLE;
  hadc.Init.LowPowerAutoPowerOff = DISABLE;
  if (HAL_ADC_Init(&hadc) != HAL_OK)
  {
    Error_Handler();
  }
  /**Configure for the selected ADC regular channel to be converted. 
  */
  sConfig.Channel = ADC_CHANNEL_0;
  sConfig.Rank = ADC_RANK_CHANNEL_NUMBER;
  if (HAL_ADC_ConfigChannel(&hadc, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /**Configure for the selected ADC regular channel to be converted. 
  */
  sConfig.Channel = ADC_CHANNEL_13;
  if (HAL_ADC_ConfigChannel(&hadc, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /**Configure for the selected ADC regular channel to be converted. 
  */
  sConfig.Channel = ADC_CHANNEL_14;
  if (HAL_ADC_ConfigChannel(&hadc, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /**Configure for the selected ADC regular channel to be converted. 
  */
  sConfig.Channel = ADC_CHANNEL_VREFINT;
  if (HAL_ADC_ConfigChannel(&hadc, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ADC_Init 2 */

  /* USER CODE END ADC_Init 2 */

}

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C1_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.Timing = 0x00000708;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }
  /**Configure Analogue filter 
  */
  if (HAL_I2CEx_ConfigAnalogFilter(&hi2c1, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
  {
    Error_Handler();
  }
  /**Configure Digital filter 
  */
  if (HAL_I2CEx_ConfigDigitalFilter(&hi2c1, 0) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C1_Init 2 */

  /* USER CODE END I2C1_Init 2 */

}

/**
  * @brief I2C2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C2_Init(void)
{

  /* USER CODE BEGIN I2C2_Init 0 */

  /* USER CODE END I2C2_Init 0 */

  /* USER CODE BEGIN I2C2_Init 1 */

  /* USER CODE END I2C2_Init 1 */
  hi2c2.Instance = I2C2;
  hi2c2.Init.Timing = 0x00000708;
  hi2c2.Init.OwnAddress1 = 0;
  hi2c2.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c2.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c2.Init.OwnAddress2 = 0;
  hi2c2.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
  hi2c2.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c2.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c2) != HAL_OK)
  {
    Error_Handler();
  }
  /**Configure Analogue filter 
  */
  if (HAL_I2CEx_ConfigAnalogFilter(&hi2c2, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
  {
    Error_Handler();
  }
  /**Configure Digital filter 
  */
  if (HAL_I2CEx_ConfigDigitalFilter(&hi2c2, 0) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C2_Init 2 */

  /* USER CODE END I2C2_Init 2 */

}

/**
  * @brief I2C3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C3_Init(void)
{

  /* USER CODE BEGIN I2C3_Init 0 */

  /* USER CODE END I2C3_Init 0 */

  /* USER CODE BEGIN I2C3_Init 1 */

  /* USER CODE END I2C3_Init 1 */
  hi2c3.Instance = I2C3;
  hi2c3.Init.Timing = 0x00000708;
  hi2c3.Init.OwnAddress1 = 0;
  hi2c3.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c3.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c3.Init.OwnAddress2 = 0;
  hi2c3.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
  hi2c3.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c3.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c3) != HAL_OK)
  {
    Error_Handler();
  }
  /**Configure Analogue filter 
  */
  if (HAL_I2CEx_ConfigAnalogFilter(&hi2c3, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
  {
    Error_Handler();
  }
  /**Configure Digital filter 
  */
  if (HAL_I2CEx_ConfigDigitalFilter(&hi2c3, 0) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C3_Init 2 */

  /* USER CODE END I2C3_Init 2 */

}

/**
  * @brief TIM3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM3_Init(void)
{

  /* USER CODE BEGIN TIM3_Init 0 */

  /* USER CODE END TIM3_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM3_Init 1 */
  uwPrescalerValue = (uint32_t)(SystemCoreClock / 2097);// uwPrescalerValue = (uint32_t)(SystemCoreClock / 2097) - 1;
  /* USER CODE END TIM3_Init 1 */
  htim3.Instance = TIM3;
  htim3.Init.Prescaler = uwPrescalerValue;
  htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim3.Init.Period = 2099;
  htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim3.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim3) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim3, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM3_Init 2 */

  /* USER CODE END TIM3_Init 2 */

}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 9600;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  huart2.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart2.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/**
  * @brief USART4 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART4_UART_Init(void)
{

  /* USER CODE BEGIN USART4_Init 0 */

  /* USER CODE END USART4_Init 0 */

  /* USER CODE BEGIN USART4_Init 1 */

  /* USER CODE END USART4_Init 1 */
  huart4.Instance = USART4;
  huart4.Init.BaudRate = 115200;
  huart4.Init.WordLength = UART_WORDLENGTH_8B;
  huart4.Init.StopBits = UART_STOPBITS_1;
  huart4.Init.Parity = UART_PARITY_NONE;
  huart4.Init.Mode = UART_MODE_TX_RX;
  huart4.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart4.Init.OverSampling = UART_OVERSAMPLING_16;
  huart4.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart4.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart4) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART4_Init 2 */

  /* USER CODE END USART4_Init 2 */

}

/**
  * @brief USART5 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART5_UART_Init(void)
{

  /* USER CODE BEGIN USART5_Init 0 */

  /* USER CODE END USART5_Init 0 */

  /* USER CODE BEGIN USART5_Init 1 */

  /* USER CODE END USART5_Init 1 */
    huart5.Instance = USART5;
    huart5.Init.BaudRate = 9600;
    huart5.Init.WordLength = UART_WORDLENGTH_8B;
    huart5.Init.StopBits = UART_STOPBITS_1;
    huart5.Init.Parity = UART_PARITY_NONE;
    huart5.Init.Mode = UART_MODE_TX_RX;
    huart5.Init.HwFlowCtl = UART_HWCONTROL_NONE;
    huart5.Init.OverSampling = UART_OVERSAMPLING_16;
    huart5.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
    huart5.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_RXOVERRUNDISABLE_INIT|UART_ADVFEATURE_DMADISABLEONERROR_INIT;
    huart5.AdvancedInit.OverrunDisable = UART_ADVFEATURE_OVERRUN_DISABLE;
    huart5.AdvancedInit.DMADisableonRxError = UART_ADVFEATURE_DMA_DISABLEONRXERROR;
  if (HAL_UART_Init(&huart5) != HAL_OK)
  {
	  Error_Handler();
  }

  /* USER CODE BEGIN USART5_Init 2 */

  /* USER CODE END USART5_Init 2 */

}

/** 
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void) 
{
  /* DMA controller clock enable */
  __HAL_RCC_DMA1_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA1_Channel1_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel1_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel1_IRQn);

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(TEMP_EXT_GPIO_Port, TEMP_EXT_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GSM_PWR_ON_GPIO_Port, GSM_PWR_ON_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(VCC_SEN_EN_GPIO_Port, VCC_SEN_EN_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(VCC_GSM_EN_GPIO_Port, VCC_GSM_EN_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(LED_GPIO_Port, LED_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pins : PC13 PC14 PC15 PC5 
                           PC6 PC7 PC8 PC9 
                           PC12 */
  GPIO_InitStruct.Pin = GPIO_PIN_13|GPIO_PIN_14|GPIO_PIN_15|GPIO_PIN_5 
                          |GPIO_PIN_6|GPIO_PIN_7|GPIO_PIN_8|GPIO_PIN_9 
                          |GPIO_PIN_12;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : PH0 PH1 */
  GPIO_InitStruct.Pin = GPIO_PIN_0|GPIO_PIN_1;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOH, &GPIO_InitStruct);

  /*Configure GPIO pin : TEMP_EXT_Pin */
  GPIO_InitStruct.Pin = TEMP_EXT_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  HAL_GPIO_Init(TEMP_EXT_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : PA1 PA4 PA6 PA7 
                           PA9 PA10 PA11 PA12 
                           PA15 */
  GPIO_InitStruct.Pin = GPIO_PIN_1|GPIO_PIN_4|GPIO_PIN_6|GPIO_PIN_7 
                          |GPIO_PIN_9|GPIO_PIN_10|GPIO_PIN_11|GPIO_PIN_12 
                          |GPIO_PIN_15;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : GSM_PWR_ON_Pin */
  GPIO_InitStruct.Pin = GSM_PWR_ON_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_OD;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
  HAL_GPIO_Init(GSM_PWR_ON_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : PB0 PB1 PB2 PB12 
                           PB13 PB5 PB8 PB9 */
  GPIO_InitStruct.Pin = GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_12 
                          |GPIO_PIN_13|GPIO_PIN_5|GPIO_PIN_8|GPIO_PIN_9;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pin : VCC_SEN_EN_Pin */
  GPIO_InitStruct.Pin = VCC_SEN_EN_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  HAL_GPIO_Init(VCC_SEN_EN_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : VCC_GSM_EN_Pin */
  GPIO_InitStruct.Pin = VCC_GSM_EN_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(VCC_GSM_EN_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : PA8 */
  GPIO_InitStruct.Pin = GPIO_PIN_8;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  GPIO_InitStruct.Alternate = GPIO_AF0_MCO;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : LED_Pin */
  GPIO_InitStruct.Pin = LED_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(LED_GPIO_Port, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */


void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc)
{
    for (int i =0; i<4; i++)
        {
          adc[i] = buffer_adc[i];
        }

        bat_raw = buffer_adc[0];
        us2_raw = buffer_adc[1];
        ph_raw  = buffer_adc[2];
        v_ref   = buffer_adc[3];
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
    UartReady = SET;

}

void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart)
{
    UartReady = SET;
    //HAL_UART_StateTypeDef state = HAL_UART_GetState(huart);

}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim3)
{
	if(st_timers.cont_time<st_timers.seconds_today)
		st_timers.cont_time++;
	if(st_timers.seconds_today<secondsOneDay)
		st_timers.seconds_today++;
/*    if(cont_time==SecondsToCommunicate)
    {
        fn_Change_Machine_State(SEND);
    }*/

}

void blink()
{
    for (int var = 0; var < 10; ++var) {
        LED_ON
        HAL_Delay(50);
        LED_OFF
        HAL_Delay(50);
    }

}

void fn_init()
{
		SEN_ON
		HAL_Delay(1000);

		st_timers.seconds_to_send = SecondsToCommunicate;

		st_timers.seconds_today = secondsOneDay-60;
		st_config_trigger_t.event_transmissions_times = 1;

		st_config_time_t.inside_wt = st_timers.seconds_to_send;
		st_config_time_t.outside_wt = st_timers.seconds_to_send;
		st_config_time_t.volume_sensor_reading = 0;
		st_config_time_t.ph_sensor_reading = 0;
		st_config_time_t.temperature_sensor_reading = 0;

		st_flags.inside_flag = true;
		st_flags.outside_flag = false;
		st_flags.alert_cj_1 = false;
		st_flags.alert_cj_2 = false;
		st_flags.alert_cj_3 = false;

		tanq_open= false;
		prevews_tanq_open = false;
		st_data_sensor_e.open_tank_counter=0;
		open_tank_timer=0;

		LED_ON
		HAL_Delay(1000);
		fn_at_uart();
		LED_CHANGE
		HAL_Delay(1000);
		fn_ID_UART();
		LED_CHANGE
		HAL_Delay(1000);
		fn_PAC_UART();
		LED_CHANGE
		HAL_Delay(1000);
		fn_volts_UART();
		LED_OFF
		fn_init_lsm();
		fn_read_sensors();
		SEN_OFF
		HAL_TIM_Base_Start_IT(&htim3);

		fn_send_start_frame();

}


	/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
