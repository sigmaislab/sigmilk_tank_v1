/*
 * sensors.c
 *
 *  Created on: 1 de ago de 2019
 *      Author: oscar
 */

#include <sensors.h>
#include <main.h>

I2C_HandleTypeDef hi2c1;
I2C_HandleTypeDef hi2c2;

/*********************READ ALL SENSOR SEQUENCE FUNCTIONS*****************************************/

void fn_read_sensors()
{
	SEN_ON
	HAL_Delay(1000);
	fn_get_adc_channels();
	fn_get_battery();
	switch (e_sensors_group)
	{
		case ALLSENSORS:
			fn_get_lsm();
			fn_get_si7021();
			fn_get_volume();
			fn_get_ph();
			fn_get_temp();
		case SENSORS_1:
			fn_get_temp();

			break;
		case SENSORS_2:
			fn_get_ph();
			break;
		case SENSORS_3:
			fn_get_volume();
			break;
		case SENSORS_1_2:
			fn_get_temp();
			fn_get_ph();
			break;
		case SENSORS_1_3:
			fn_get_temp();
			fn_get_volume();
			break;
		case SENSORS_3_2:
			fn_get_volume();
			fn_get_ph();
			break;
		default:
			break;
		}
	SEN_OFF
	st_data_sensor_e.open_tank_min = open_tank_timer/60;
}

/*********************PH FUNCTIONS*****************************************/
void fn_get_ph()
{
	st_data_sensor_previwes_e.ph_value = st_data_sensor_e.ph_value;
/*
    ph_calibrate = 7;
    ph_volts = (ph_raw*5)/4095;
    //ph_volts-=0.05;


    ph_float = ph_calibrate +((2.5-ph_volts)/0.18);// (ph_volts*(5.7)/6);
*/

	st_data_sensor_e.ph_value = 7;
}

/*******************TEMPERATURE DEFINITION FUNCTIONS**************************/
void fn_get_temp()
{
	st_data_sensor_previwes_e.tpl_value = st_data_sensor_e.tpl_value;
	st_data_sensor_previwes_e.ds18b20_value = st_data_sensor_e.ds18b20_value;
    //Temperature
    //tmp_raw
	fn_get_ds18b20();
    if (Temperature !=0 && temperature_degC != 0)
    	st_data_sensor_e.tpe_value = ((Temperature+temperature_degC)/2)+128;
    else if (Temperature == 0 && temperature_degC !=0)
    	st_data_sensor_e.tpe_value = temperature_degC+128;
    else if (Temperature !=0 && temperature_degC == 0)
    	st_data_sensor_e.tpe_value = Temperature+128;
    else
    	st_data_sensor_e.tpe_value = 0;

    st_data_sensor_e.tpl_value = (temp_ds18b20+128)*100;
    st_data_sensor_e.ds18b20_value = temp_ds18b20;

}

/*******************VOLUME FUNCTIONS*****************************************/
void fn_get_volume()
{
	st_data_sensor_previwes_e.vol_value = st_data_sensor_e.vol_value;
    us2_volt=((us2_raw*3)/4095);//1023); //4096 = 12 bits and 1023 = 10 bits
    us2_value = (int) ((us2_volt*2.54)/0.006445);
    if(us2_value > 200)
        st_data_sensor_e.vol_value = 200;
    else
    	st_data_sensor_e.vol_value = us2_value;
}

/*******************BATTERY FUNCTIONS*****************************************/
void fn_get_battery()
{

    bat_volt = (4400/4095)*bat_raw;
    bat_volt=1*(int)bat_volt;
    fn_volts_UART();
    if(bat_volt>4000 || bat_volt<1000)
    {
    	st_data_sensor_e.bat_value = (int)uart_volt/100;
        }
    else
    	st_data_sensor_e.bat_value = (int)bat_volt/100;
}

/*******************SI7020 FUNCTIONS*****************************************/
void fn_get_si7021()
{

        static const uint16_t I2C_ADDR = (0x40);           // Si7021 I2C address
        unsigned char buffer[5];
        //MX_I2C2_Init();
       /* HAL_Delay(300);
        //reset
        buffer[0]=0xFE;
        HAL_I2C_Master_Transmit(&hi2c2,I2C_ADDR<<1,buffer,1,100);
       */
        HAL_I2C_Init(&hi2c2);
        HAL_Delay(1000);
        buffer[0]=0x02; //Pointer
        buffer[1]=0;    //MSB byte
        buffer[2]=0;    //LSB byte
        HAL_I2C_Master_Transmit(&hi2c2,I2C_ADDR<<1,buffer,3,100);
        HAL_Delay(30);
        //Trigger Temperature measurement
        buffer[0]=0xF3;
        HAL_I2C_Master_Transmit(&hi2c2,I2C_ADDR<<1,buffer,1,100);

        HAL_Delay(30);

        HAL_I2C_Master_Receive(&hi2c2,I2C_ADDR<<1,buffer,2,100);
        //buffer[0] : MSB data
        //buffer[1] : LSB data

        rawT = buffer[0]<<8 | buffer[1];    //combine 2 8-bit into 1 16bit


        Temperature = ((float)rawT/65536)*175.5-46.85;

        //Trigger Humidity measurement
        buffer[0]=0xF5;
        HAL_I2C_Master_Transmit(&hi2c2,I2C_ADDR<<1,buffer,1,100);

        HAL_Delay(30);

        HAL_I2C_Master_Receive(&hi2c2,I2C_ADDR<<1,buffer,2,100);
        //buffer[0] : MSB data
        //buffer[1] : LSB data

        rawH = buffer[0]<<8 | buffer[1];    //combine 2 8-bit into 1 16bit

        Humidity = ((float)rawH*125/65536)-6;
        //reset
        buffer[0]=0xFE;
        HAL_I2C_Master_Transmit(&hi2c2,I2C_ADDR<<1,buffer,1,100);

        HAL_Delay(10);


        st_data_sensor_e.hum_value = round(Humidity);

            //tmp_value = (Temperature);

}

/*******************LSM303 FUNCTIONS*****************************************/
void fn_init_lsm(){

    static axis3bit16_t data_raw_acceleration;
    static axis3bit16_t data_raw_magnetic;
    static axis1bit16_t data_raw_temperature;
    static float acceleration_mg[3];
   static float magnetic_mG[3];
    static uint8_t whoamI, rst;
    //int16_t a_x, a_y, a_z;
    //int16_t  pitch, roll, pitch_y, roll_x, yaw, yaw_y;


    lsm303agr_ctx_t dev_ctx_xl;
      dev_ctx_xl.write_reg = platform_write;
      dev_ctx_xl.read_reg = platform_read;
      dev_ctx_xl.handle = (void*)LSM303AGR_I2C_ADD_XL;

    lsm303agr_ctx_t dev_ctx_mg;
      dev_ctx_mg.write_reg = platform_write;
      dev_ctx_mg.read_reg = platform_read;
      dev_ctx_mg.handle = (void*)LSM303AGR_I2C_ADD_MG;

      /*
       *  Check device ID
       */
      whoamI = 0;
      lsm303agr_xl_device_id_get(&dev_ctx_xl, &whoamI);
      if ( whoamI != LSM303AGR_ID_XL ){
          for (int var = 0; var < 20; ++var) {

            HAL_Delay(100);

        }
      }

       //while(1); /*manage here device not found */

/*      whoamI = 0;
      lsm303agr_mag_device_id_get(&dev_ctx_mg, &whoamI);
      if ( whoamI != LSM303AGR_ID_MG )*/

        //while(1); /*manage here device not found */

      /*
       *  Restore default configuration for magnetometer
       */
    lsm303agr_mag_reset_set(&dev_ctx_mg, PROPERTY_ENABLE);
      do {
         lsm303agr_mag_reset_get(&dev_ctx_mg, &rst);
      } while (rst);

      /*
       *  Enable Block Data Update
       */
      lsm303agr_xl_block_data_update_set(&dev_ctx_xl, PROPERTY_ENABLE);
      lsm303agr_mag_block_data_update_set(&dev_ctx_mg, PROPERTY_ENABLE);
      /*
       * Set Output Data Rate
       */
      lsm303agr_xl_data_rate_set(&dev_ctx_xl, LSM303AGR_XL_ODR_1Hz);
      lsm303agr_mag_data_rate_set(&dev_ctx_mg, LSM303AGR_MG_ODR_10Hz);
      /*
       * Set accelerometer full scale
       */
      lsm303agr_xl_full_scale_set(&dev_ctx_xl, LSM303AGR_2g);
      /*
       * Set / Reset magnetic sensor mode
       */
      lsm303agr_mag_set_rst_mode_set(&dev_ctx_mg, LSM303AGR_SENS_OFF_CANC_EVERY_ODR);
      /*
       * Enable temperature compensation on mag sensor
       */
      lsm303agr_mag_offset_temp_comp_set(&dev_ctx_mg, PROPERTY_ENABLE);
      /*
       * Enable temperature sensor
       */
      lsm303agr_temperature_meas_set(&dev_ctx_xl, LSM303AGR_TEMP_ENABLE);
      /*
       * Set device in continuos mode
       */
      lsm303agr_xl_operating_mode_set(&dev_ctx_xl, LSM303AGR_HR_12bit);
      /*
       * Set magnetometer in continuos mode
       */
      lsm303agr_mag_operating_mode_set(&dev_ctx_mg, LSM303AGR_CONTINUOUS_MODE);

      /*
       * Read samples in polling mode (no int)
       */
     /*
         * Read output only if new value is available
         */
        lsm303agr_reg_t reg;

        lsm303agr_xl_status_get(&dev_ctx_xl, &reg.status_reg_a);

        if (reg.status_reg_a.zyxda)
        {
          /* Read accelerometer data */
          memset(data_raw_acceleration.u8bit, 0x00, 3*sizeof(int16_t));
          lsm303agr_acceleration_raw_get(&dev_ctx_xl, data_raw_acceleration.u8bit);
          acceleration_mg[0] = LSM303AGR_FROM_FS_2g_HR_TO_mg( data_raw_acceleration.i16bit[0] );
          acceleration_mg[1] = LSM303AGR_FROM_FS_2g_HR_TO_mg( data_raw_acceleration.i16bit[1] );
          acceleration_mg[2] = LSM303AGR_FROM_FS_2g_HR_TO_mg( data_raw_acceleration.i16bit[2] );

           a_x =  acceleration_mg[0];
           a_y =  acceleration_mg[1];
           a_z =  acceleration_mg[2];

           pitch_y = (atan2(a_y, sqrt(a_x*a_x + a_z*a_z))*180.0)/M_PI; //angulo eixo y
           pitch =   (atan2(a_x, sqrt(a_y*a_y + a_z*a_z))*180.0)/M_PI; // angulo eixo x

           roll =    (atan2(a_y, a_z)*180.0)/M_PI;
           roll_x =  (atan2(a_x, a_z)*180.0)/M_PI;

           yaw =     180 *(atan2(a_z,sqrt(a_x*a_x +  a_z* a_z)))/M_PI;
           yaw_y =   180 *(atan2(a_z,sqrt(a_x*a_x +  a_z* a_z)))/M_PI;
  /*       // ang_raw =(pitch_y+ roll+roll_x+yaw+yaw_y+pitch)/6;
            ang_value = pitch/10;
            if (ang_value < 0)
              ang_value *=(-1);*/
        }

      lsm303agr_mag_status_get(&dev_ctx_mg, &reg.status_reg_m);
        if (reg.status_reg_m.zyxda)
        {
           //Read magnetic field data
          memset(data_raw_magnetic.u8bit, 0x00, 3*sizeof(int16_t));
          lsm303agr_magnetic_raw_get(&dev_ctx_mg, data_raw_magnetic.u8bit);
          magnetic_mG[0] = LSM303AGR_FROM_LSB_TO_mG( data_raw_magnetic.i16bit[0]);
          magnetic_mG[1] = LSM303AGR_FROM_LSB_TO_mG( data_raw_magnetic.i16bit[1]);
          magnetic_mG[2] = LSM303AGR_FROM_LSB_TO_mG( data_raw_magnetic.i16bit[2]);
          m_x = magnetic_mG[0];
          m_y = magnetic_mG[1];
          m_z = magnetic_mG[2];

        }
        lsm303agr_temp_data_ready_get(&dev_ctx_xl, &reg.byte);
        if (reg.byte)
        {
          /* Read temperature data */
          memset(data_raw_temperature.u8bit, 0x00, sizeof(int16_t));
          lsm303agr_temperature_raw_get(&dev_ctx_xl, data_raw_temperature.u8bit);
          temperature_degC = LSM303AGR_FROM_LSB_TO_degC_HR( data_raw_temperature.i16bit );
          //tmp_raw = temperature_degC;
          //tmp_value = tmp_raw;
          //sprintf((char*)tx_buffer, "Temperature [degC]:%6.2f\r\n", temperature_degC );
        }
}

void fn_get_lsm()
{
    static axis3bit16_t data_raw_acceleration;
    static axis3bit16_t data_raw_magnetic;
    static axis1bit16_t data_raw_temperature;
    static float acceleration_mg[3];
    static uint8_t whoamI;
    static float magnetic_mG[3];

    lsm303agr_ctx_t dev_ctx_xl;
    dev_ctx_xl.write_reg = platform_write;
    dev_ctx_xl.read_reg = platform_read;
    dev_ctx_xl.handle = (void*)LSM303AGR_I2C_ADD_XL;
    whoamI = 0;
         lsm303agr_xl_device_id_get(&dev_ctx_xl, &whoamI);
         if ( whoamI != LSM303AGR_ID_XL ){
             //ang_value=0;
             temperature_degC=0;
         }else{

    lsm303agr_ctx_t dev_ctx_mg;
    dev_ctx_mg.write_reg = platform_write;
    dev_ctx_mg.read_reg = platform_read;
    dev_ctx_mg.handle = (void*)LSM303AGR_I2C_ADD_MG;

      /*
       * Read samples in polling mode (no int)
       */
     /*
         * Read output only if new value is available
         */
        lsm303agr_reg_t reg;

        lsm303agr_xl_status_get(&dev_ctx_xl, &reg.status_reg_a);

        if (reg.status_reg_a.zyxda)
        {
          /* Read accelerometer data */
          memset(data_raw_acceleration.u8bit, 0x00, 3*sizeof(int16_t));
          lsm303agr_acceleration_raw_get(&dev_ctx_xl, data_raw_acceleration.u8bit);
          acceleration_mg[0] = LSM303AGR_FROM_FS_2g_HR_TO_mg( data_raw_acceleration.i16bit[0] );
          acceleration_mg[1] = LSM303AGR_FROM_FS_2g_HR_TO_mg( data_raw_acceleration.i16bit[1] );
          acceleration_mg[2] = LSM303AGR_FROM_FS_2g_HR_TO_mg( data_raw_acceleration.i16bit[2] );

          a_x =  acceleration_mg[0];
          a_y =  acceleration_mg[1];
          a_z =  acceleration_mg[2];
           pitch_y = (atan2(a_y, sqrt(a_x*a_x + a_z*a_z))*180.0)/M_PI; //angulo eixo y
          pitch =   (atan2(a_x, sqrt(a_y*a_y + a_z*a_z))*180.0)/M_PI; // angulo eixo x
           roll =    (atan2(a_y, a_z)*180.0)/M_PI;
           roll_x =  (atan2(a_x, a_z)*180.0)/M_PI;
           yaw =     180 *(atan2(a_z,sqrt(a_x*a_x +  a_z* a_z)))/M_PI;
           yaw_y =   180 *(atan2(a_z,sqrt(a_x*a_x +  a_z* a_z)))/M_PI;
          /*ang_raw =(pitch_y+ roll+roll_x+yaw+yaw_y+pitch)/6;
          ang_value = pitch/10;
          if (ang_value < 0)
             ang_value *=(-1);*/
        }

        lsm303agr_mag_status_get(&dev_ctx_mg, &reg.status_reg_m);
        if (reg.status_reg_m.zyxda)
        {
          // Read magnetic field data
          memset(data_raw_magnetic.u8bit, 0x00, 3*sizeof(int16_t));
          lsm303agr_magnetic_raw_get(&dev_ctx_mg, data_raw_magnetic.u8bit);
          magnetic_mG[0] = LSM303AGR_FROM_LSB_TO_mG( data_raw_magnetic.i16bit[0]);
          magnetic_mG[1] = LSM303AGR_FROM_LSB_TO_mG( data_raw_magnetic.i16bit[1]);
          magnetic_mG[2] = LSM303AGR_FROM_LSB_TO_mG( data_raw_magnetic.i16bit[2]);

          m_x = magnetic_mG[0];
          m_y = magnetic_mG[1];
          m_z = magnetic_mG[2];
        }
        lsm303agr_temp_data_ready_get(&dev_ctx_xl, &reg.byte);
        if (reg.byte)
        {
          /* Read temperature data */
          memset(data_raw_temperature.u8bit, 0x00, sizeof(int16_t));
          lsm303agr_temperature_raw_get(&dev_ctx_xl, data_raw_temperature.u8bit);
          temperature_degC = LSM303AGR_FROM_LSB_TO_degC_HR( data_raw_temperature.i16bit );
          //sprintf((char*)tx_buffer, "Temperature [degC]:%6.2f\r\n", temperature_degC );
        }
        if(pitch_y >50){
            if(tanq_open == false)
            {st_data_sensor_e.open_tank_counter++;}
            tanq_open = true;
        }
            if(pitch_y<10){
            	tanq_open = false;
        }
       }
}


 int32_t platform_write(void *handle, uint8_t Reg, uint8_t *Bufp,
                              uint16_t len)
{
  uint32_t i2c_add = (uint32_t)handle;
  if (i2c_add == LSM303AGR_I2C_ADD_XL)
  {
    /* enable auto incremented in multiple read/write commands */
    Reg |= 0x80;
  }
  HAL_I2C_Mem_Write(&hi2c1, i2c_add, Reg,
                    I2C_MEMADD_SIZE_8BIT, Bufp, len, 1000);
  return 0;
}

 int32_t platform_read(void *handle, uint8_t Reg, uint8_t *Bufp,
                             uint16_t len)
{
  uint32_t i2c_add = (uint32_t)handle;
  if (i2c_add == LSM303AGR_I2C_ADD_XL)
  {
    /* enable auto incremented in multiple read/write commands */
    Reg |= 0x80;
  }
  HAL_I2C_Mem_Read(&hi2c1, (uint8_t) i2c_add, Reg,
                   I2C_MEMADD_SIZE_8BIT, Bufp, len, 1000);
  return 0;
}
