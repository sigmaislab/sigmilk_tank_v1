/*
 * sigfox.c
 *
 *  Created on: 1 de ago de 2019
 *      Author: oscar
 */


#include <sigfox.h>



//******************** SIGFOX FUNCTIONS ************************************
void fn_init_uart()
{
        char ok[4];
        HAL_UART_Transmit(&huart5,(uint8_t*)"AT$P=0\r\n",8,100);
        HAL_UART_Receive(&huart5,(uint8_t*)ok,4,100);
        HAL_Delay(100);
        HAL_UART_Transmit(&huart5,(uint8_t*)"AT$DR=905200000\r\n",17,100);
        HAL_UART_Receive(&huart5,(uint8_t*)ok,4,100);
        HAL_Delay(100);
        HAL_UART_Transmit(&huart5,(uint8_t*)"AT$IF=902200000\r\n",17,100);
        HAL_UART_Receive(&huart5,(uint8_t*)ok,4,100);
        HAL_Delay(100);
        HAL_UART_Transmit(&huart5,(uint8_t*)"AT$WR\r\n",7,100);
        HAL_UART_Receive(&huart5,(uint8_t*)ok,4,100);
        HAL_Delay(100);
        HAL_UART_Transmit(&huart5,(uint8_t*)"AT$RC\r\n",7,100);
        HAL_UART_Receive(&huart5,(uint8_t*)ok,4,100);
        HAL_Delay(100);


		#ifdef DONGLE_KEY
				//public key or private key
				HAL_UART_Transmit(&huart5,(uint8_t*)"ATS410=1\r\n",11,50); //ATS410=1 private key ; ATS410=0 public key
				//HAL_UART_Receive(&huart5,(uint8_t*)ok,4,10);
				 HAL_Delay(1000);
		#endif
}

void fn_ID_UART()
{

    //clear_char(buffer_uart);
    //clear_char(id_uart);

    char command[9]="AT$I=10\r\n";


    HAL_UART_Transmit_IT(&huart5,(uint8_t*)command,9);
    //HAL_UART_Receive_IT(&huart5,(uint8_t*)id_uart,strlen(id_uart));
    HAL_UART_Receive_IT(&huart5,(uint8_t*)id_uart,10);

      hexDec(id_uart);
      //id_sigfox_int = (int) decimal;
    //HAL_Delay(50);
    //RemoveSpaces(buffer_uart);
    //strcpy(id_uart,buffer_uart);


}

void fn_PAC_UART()
{
    //clear_char(buffer_uart);
    //clear_char(pac_uart);

    char command[10]="AT$I=11\r\n";
    HAL_UART_Transmit_IT(&huart5,(uint8_t*)command,strlen(command));
    //HAL_UART_Receive_IT(&huart5,(uint8_t*)id_uart,strlen(id_uart));
    HAL_UART_Receive_IT(&huart5,(uint8_t*)pac_uart,18);
     hexDec(pac_uart);
    // pac_sigfox_int = (int) decimal;
    //HAL_UART_Transmit(&huart5,(uint8_t*)command,10,200);
    //HAL_Delay(10);
    //HAL_UART_Receive(&huart5,(uint8_t*)buffer_uart,buffer_size,1000);

    //HAL_Delay(50);
    //RemoveSpaces(buffer_uart);
    //bstrcpy(pac_uart,buffer_uart);
}

void fn_at_uart()
{
    char command[4]="AT\r\n";

    HAL_UART_Transmit_IT(&huart5,(uint8_t*)command,4);
      HAL_UART_Receive_IT(&huart5,(uint8_t*)at_uart,4);
}

void fn_uart_sleep()
{

 //HAL_UART_Transmit_IT(&huart5,(uint8_t*)"AT$P=2\r\n",8);

}

void fn_reset_uart()
{
/*
    HAL_GPIO_WritePin(WS_RST_GPIO_Port,WS_RST_Pin,0);
    HAL_Delay(50);
    HAL_GPIO_DeInit(WS_RST_GPIO_Port,WS_RST_Pin);*/

}

void fn_wakeup_uart()
{
 /*   HAL_GPIO_WritePin(WS_WAKEUP_GPIO_Port,WS_WAKEUP_Pin,1);
    HAL_Delay(50);
    HAL_GPIO_WritePin(WS_WAKEUP_GPIO_Port,WS_WAKEUP_Pin,0);
    HAL_Delay(50);
    HAL_GPIO_WritePin(WS_WAKEUP_GPIO_Port,WS_WAKEUP_Pin,1);*/
}

void fn_temp_UART()
{
/*  int tempo = 5;
    if (tmp_value!=0 || tempo!=0)
    {*/
    char temp_uart[7];
    char command[8]="AT$T?\r\n";
    HAL_UART_Transmit_IT(&huart5,(uint8_t*)command,8);
    HAL_Delay(10);
    HAL_UART_Receive_IT(&huart5,(uint8_t*)temp_uart,7);
    HAL_Delay(50);
    uart_temp = atof (temp_uart);
/*  tempo--;
    }
        if(tmp_value>100)
            tmp_value/=10;
        else
        {
            if (tmp_value < 10 || tmp_value > 40)
                tmp_value = 26;
        }*/
    HAL_Delay(50);
}

void fn_volts_UART()
{
/*  int tempo = 5;
    if (tmp_value!=0 || tempo!=0)
    {*/
    char volts_uart[10];
    char command[8]="AT$V?\r\n";
    HAL_UART_Transmit(&huart5,(uint8_t*)command,8,100);
    HAL_UART_Receive(&huart5,(uint8_t*)volts_uart,10,100);
    HAL_Delay(50);


    uart_volt = atof (volts_uart);
}


//#################### PAYLOAD FUNCTIONS #####################################

void fn_send_report_frame()
{
    fn_init_uart();
    char data_frame[33]={0};
    char ok[4]={0};
    //char payload_values2[24]={0};
    //fn_mount_values(payload_values2);
    data_frame[0]=65;
    data_frame[1]=84;
    data_frame[2]=36;
    data_frame[3]=83;
    data_frame[4]=70;
    data_frame[5]=61;
  /*  for (int var = 0; var < 24; ++var)
    {
        payload_completo2[var+6] = payload_values2[var];
    }*/

	decHex(hea_value);
	size_2();
	strcat(data_frame,buff);

	decHex(st_data_sensor_e.bat_value);
	size_2();
	strcat(data_frame,buff);

	decHex(st_data_sensor_e.tpe_value);
	size_2();
	strcat(data_frame,buff);

   decHex(st_data_sensor_e.tpl_value);
   size_4();
   strcat(data_frame,buff);

	decHex(st_data_sensor_e.hum_value);
	size_2();
	strcat(data_frame,buff);

	decHex(st_data_sensor_e.vol_value);
	size_2();
	strcat(data_frame,buff);

	decHex(st_data_sensor_e.ph_value);
	size_2();
	strcat(data_frame,buff);

	decHex(st_data_sensor_e.open_tank_counter);
	size_4();
	strcat(data_frame,buff);

	decHex(st_data_sensor_e.open_tank_min);
	size_4();
	strcat(data_frame,buff);

	data_frame[30] = 13;
	data_frame[31] = 10;
	data_frame[32] = 0;
    HAL_UART_Transmit(&huart5,(uint8_t*)data_frame,33,200);
    HAL_UART_Receive(&huart5,(uint8_t*)ok,4,100);

}

void fn_send_alert_frame()
{
    fn_init_uart();
    char alarm_frame[33]={0};
    char ok[4]={0};
    char alert_header[8]={0};
    //char payload_values2[24]={0};
    //fn_mount_values(payload_values2);
    //AT$SF=
    alarm_frame[0]=65;
    alarm_frame[1]=84;
    alarm_frame[2]=36;
    alarm_frame[3]=83;
    alarm_frame[4]=70;
    alarm_frame[5]=61;

	  alert_header[0] = 49;
	  alert_header[1] = 48;
	  alert_header[2] = 48;
	  alert_header[3] = 48;
	  if(st_flags.alert_cj_1 == true)
	  alert_header[4] = 49;
	  if(st_flags.alert_cj_1 == false)
	  alert_header[4] = 48;
	  if(st_flags.alert_cj_2 == true)
	  alert_header[5] = 49;
	  if(st_flags.alert_cj_2 == false)
	  alert_header[6] = 48;
	  if(st_flags.alert_cj_3 == true)
	  alert_header[6] = 49;
	  if(st_flags.alert_cj_3 == false)
	  alert_header[6] = 48;
	  alert_header[7] = 48;
	  hea_value = bin_to_dec(alert_header);

  /*  for (int var = 0; var < 24; ++var)
    {
        payload_completo2[var+6] = payload_values2[var];
    }*/

	decHex(hea_value);
	size_2();
	strcat(alarm_frame,buff);

	decHex(st_data_sensor_e.bat_value);
	size_2();
	strcat(alarm_frame,buff);

	decHex(st_data_sensor_e.tpe_value);
	size_2();
	strcat(alarm_frame,buff);

   decHex(st_data_sensor_e.tpl_value);
   size_4();
   strcat(alarm_frame,buff);

	decHex(st_data_sensor_e.hum_value);
	size_2();
	strcat(alarm_frame,buff);

	decHex(st_data_sensor_e.vol_value);
	size_2();
	strcat(alarm_frame,buff);

	decHex(st_data_sensor_e.ph_value);
	size_2();
	strcat(alarm_frame,buff);

	decHex(st_data_sensor_e.open_tank_counter);
	size_4();
	strcat(alarm_frame,buff);

	decHex(st_data_sensor_e.open_tank_min);
	size_4();
	strcat(alarm_frame,buff);

	alarm_frame[30] = 13;
	alarm_frame[31] = 10;
	alarm_frame[32] = 0;
    HAL_UART_Transmit(&huart5,(uint8_t*)alarm_frame,33,200);
    HAL_UART_Receive(&huart5,(uint8_t*)ok,4,100);

}

void fn_send_daily_frame()
{
		hea_value=3;
	    char report_frame[35]={0};
	    char config[64]={0};
	    char buff_downlink[23]={0};
	    char buff_dec[64]={0};
	    char time_stamp [6]={0};
	    char worktime_buff[8]={0};
	    char worktime_dec[32]={0};
	    int s_d_on,s_d_tw,s_d_th,e_d_on,e_d_tw,e_d_th;
	    char dayOneS[5]={0};
	    char dayOneE[5]={0};
	    char dayTwoS[5]={0};
	    char dayTwoE[5]={0};
	    char dayThreeS[5]={0};
	    char dayThreeE[5]={0};

	    //int  timebyte_int, undTimeByte_int,time_byte_value=0;
	    for (int var = 0; var < 3; var++)
	    {
	    	fn_init_uart();
	    	   // char ok[4]={0};
	    	    //AT$SF=
	    	    report_frame[0]=65;
	    	    report_frame[1]=84;
	    	    report_frame[2]=36;
	    	    report_frame[3]=83;
	    	    report_frame[4]=70;
	    	    report_frame[5]=61;
	    		decHex(hea_value);
	    		size_2();
	    		strcat(report_frame,buff);

	    		decHex(st_data_sensor_e.bat_value);
	    		size_2();
	    		strcat(report_frame,buff);

	    		decHex(st_data_sensor_e.tpe_value);
	    		size_2();
	    		strcat(report_frame,buff);

	    	   decHex(st_data_sensor_e.tpl_value);
	    	   size_4();
	    	   strcat(report_frame,buff);

	    		decHex(st_data_sensor_e.hum_value);
	    		size_2();
	    		strcat(report_frame,buff);

	    		decHex(st_data_sensor_e.vol_value);
	    		size_2();
	    		strcat(report_frame,buff);

	    		decHex(st_data_sensor_e.ph_value);
	    		size_2();
	    		strcat(report_frame,buff);

	    		decHex(st_data_sensor_e.open_tank_counter);
	    		size_4();
	    		strcat(report_frame,buff);

	    		decHex(st_data_sensor_e.open_tank_min);
	    		size_4();
	    		strcat(report_frame,buff);

	    		report_frame[30] = 44;
	    		report_frame[31] = 49;
	    		report_frame[32] = 13;
	    		report_frame[33] = 10;
	    		report_frame[34] = 0;

	    	HAL_UART_Transmit(&huart5,(uint8_t*)report_frame,35,100);
	    	//int var = 20+st_timers.cont_time;
	    	HAL_Delay(28000);
/*	    	while(st_timers.cont_time<var)
	    	{

	    	}*/
	        HAL_UART_Receive(&huart5, (uint8_t *)config,64,7000);

	        sigfox_downlink = false;
	        find_between("RX=","\r",config,buff_downlink);
	        if(strlen(buff_downlink)>=15)
	        {
	            var=3;
	            sigfox_downlink = true;
	            RemoveSpaces(buff_downlink);
	            hexBin (buff_downlink,buff_dec);

	            if(buff_downlink[0] == 48)
	            	config_flag = false;
	            if(buff_downlink[0] == 49)
	            	config_flag = true;

	            time_stamp[0] = buff_downlink[6];
	            time_stamp[1] = buff_downlink[7];
	            time_stamp[2] = buff_downlink[4];
	            time_stamp[3] = buff_downlink[5];
	            time_stamp[4] = buff_downlink[2];
	            time_stamp[5] = buff_downlink[3];



				worktime_buff[0] = buff_downlink[14];
				worktime_buff[1] = buff_downlink[15];
				worktime_buff[2] = buff_downlink[12];
				worktime_buff[3] = buff_downlink[13];
				worktime_buff[4] = buff_downlink[10];
				worktime_buff[5] = buff_downlink[11];
				worktime_buff[6] = buff_downlink[8];
				worktime_buff[7] = buff_downlink[9];

				hexBin (worktime_buff,worktime_dec);

			     dayOneS[0]=worktime_dec[2];
			     dayOneS[1]=worktime_dec[3];
			     dayOneS[2]=worktime_dec[4];
			     dayOneS[3]=worktime_dec[5];
			     dayOneS[4]=worktime_dec[6];

			     dayOneE[0]=worktime_dec[7];
			     dayOneE[1]=worktime_dec[8];
			     dayOneE[2]=worktime_dec[9];
			     dayOneE[3]=worktime_dec[10];
			     dayOneE[4]=worktime_dec[11];

			     dayTwoS[0]=worktime_dec[12];
			     dayTwoS[1]=worktime_dec[13];
			     dayTwoS[2]=worktime_dec[14];
			     dayTwoS[3]=worktime_dec[15];
			     dayTwoS[4]=worktime_dec[16];

			     dayTwoE[0]=worktime_dec[17];
			     dayTwoE[1]=worktime_dec[18];
			     dayTwoE[2]=worktime_dec[19];
			     dayTwoE[3]=worktime_dec[20];
			     dayTwoE[4]=worktime_dec[21];

			     dayThreeS[0]=worktime_dec[22];
			     dayThreeS[1]=worktime_dec[23];
			     dayThreeS[2]=worktime_dec[24];
			     dayThreeS[3]=worktime_dec[25];
			     dayThreeS[4]=worktime_dec[26];

			     dayThreeE[0]=worktime_dec[27];
			     dayThreeE[1]=worktime_dec[28];
			     dayThreeE[2]=worktime_dec[29];
			     dayThreeE[3]=worktime_dec[30];
			     dayThreeE[4]=worktime_dec[31];

			     s_d_on = atoi(dayOneE);
				 s_d_tw = atoi(dayTwoE);
				 s_d_th = atoi(dayThreeE);
				 e_d_on = atoi(dayOneS);
				 e_d_tw = atoi(dayTwoS);
				 e_d_th = atoi(dayThreeS);

				st_worktime.start_day_one =  	binaryToDec(s_d_on);
				st_worktime.start_day_two =  	binaryToDec(s_d_tw);
				st_worktime.start_day_three =  	binaryToDec(s_d_th);
				st_worktime.end_day_one = 		binaryToDec(e_d_on);
				st_worktime.end_day_two = 		binaryToDec(e_d_tw);
				st_worktime.end_day_three = 	binaryToDec(e_d_th);


	            TimeStamp     =   hexDec(time_stamp); //timestamp minutes-sigmais protocol

	            st_timers.seconds_today = fn_get_seconsForTimeStemp(TimeStamp); //seconds elapsed in the day

	            e_day_to_work = DAYONE;
	            }
	    }
	    if(e_day_to_work == DAYTHREE)
	    {
	    	 e_day_to_work = NODAY;
	    }
	    if(sigfox_downlink == false)
	    {
	    	e_day_to_work++;
	    }
	     fn_uart_sleep();
}

void fn_send_config_frame()
{
			hea_value=4;
		    char request_frame[13]={0};
		    char config[64]={0};
		    char buff_downlink[23]={0};

		    char temp_max[1]={0};
		    char temp_min[1]={0};
		    char vol_max[1]={0};
		    char vol_min[1]={0};
		    char t_transm2[1]={0};
		    char t_transm1[1]={0};

		    char buffer[6]={0};
		    char buffer_bin[24]={0};

		    for (int var = 0; var < 3; var++)
		    {
		    	fn_init_uart();
		    	request_frame[0]=65;
		    	request_frame[1]=84;
		    	request_frame[2]=36;
		    	request_frame[3]=83;
		    	request_frame[4]=70;
		    	request_frame[5]=61;
				decHex(hea_value);
				size_2();
				strcat(request_frame,buff);
				request_frame[8] = 44;
				request_frame[9] = 49;
				request_frame[10] = 13;
				request_frame[11] = 10;
				request_frame[12] = 0;

		    	HAL_UART_Transmit(&huart5,(uint8_t*)request_frame,13,100);
		    	HAL_Delay(30000);
		        HAL_UART_Receive(&huart5, (uint8_t *)config,64,6000);
		        find_between("RX=","\r",config,buff_downlink);
		        if(strlen(buff_downlink)>=15)
		        {
		            st_config_trigger_t.threshold_temperature = 0;
					st_config_trigger_t.threshold_ph = 0;
					st_config_trigger_t.threshold_volume = 0;

					st_config_time_t.temperature_sensor_reading = 0;
					st_config_time_t.ph_sensor_reading = 0;
					st_config_time_t.volume_sensor_reading = 0;

					st_config_trigger_t.event_transmissions_times = 0;

		            var=3;
		            RemoveSpaces(buff_downlink);

		             temp_max[0] = buff_downlink[4];
		             temp_min[0] = (buff_downlink[5]);

		             vol_max[0] = (buff_downlink[6]);
		             vol_min[0] = (buff_downlink[7]);

		             t_transm2[0] = (buff_downlink[8]);
		             t_transm1[0] = (buff_downlink[9]);

		             buffer[0] = buff_downlink[10];
		             buffer[1] = buff_downlink[11];
		             buffer[2] = buff_downlink[12];
		             buffer[3] = buff_downlink[13];
		             buffer[4] = buff_downlink[14];
		             buffer[5] = buff_downlink[15];

		             hexBin(buffer,buffer_bin);

		             if (buffer_bin[23] == 49)
		            	 st_config_time_t.temperature_sensor_reading+=1;
		             if (buffer_bin[22] == 49)
		            	 st_config_time_t.temperature_sensor_reading+=2;
		             if (buffer_bin[21] == 49)
		            	 st_config_time_t.temperature_sensor_reading+=4;

		             if (buffer_bin[20] == 49)
		            	 st_config_time_t.ph_sensor_reading+=1;
		             if (buffer_bin[19] == 49)
		            	 st_config_time_t.ph_sensor_reading+=2;
		             if (buffer_bin[18] == 49)
		            	 st_config_time_t.ph_sensor_reading+=4;

		             if (buffer_bin[17] == 49)
		            	 st_config_time_t.volume_sensor_reading+=1;
		             if (buffer_bin[16] == 49)
		            	 st_config_time_t.volume_sensor_reading+=2;
		             if (buffer_bin[15] == 49)
		            	 st_config_time_t.volume_sensor_reading+=4;

		             if (buffer_bin[14] == 49)
		            	 st_config_trigger_t.event_transmissions_times+=1;
		             if (buffer_bin[13] == 49)
		            	 st_config_trigger_t.event_transmissions_times+=2;

		             if (buffer_bin[12] == 49)
		            	 st_config_trigger_t.threshold_temperature+=1;
		             if (buffer_bin[11] == 49)
		            	 st_config_trigger_t.threshold_temperature+=2;
		             if (buffer_bin[10] == 49)
		            	 st_config_trigger_t.threshold_temperature+=4;
		             if (buffer_bin[9] == 49)
		            	 st_config_trigger_t.threshold_temperature+=8;


		             if (buffer_bin[8] == 49)
		            	 st_config_trigger_t.threshold_ph+=1;
		             if (buffer_bin[7] == 49)
		            	 st_config_trigger_t.threshold_ph+=2;
		             if (buffer_bin[6] == 49)
		            	 st_config_trigger_t.threshold_ph+=4;
		             if (buffer_bin[5] == 49)
		            	 st_config_trigger_t.threshold_ph+=8;

		             if (buffer_bin[4] == 49)
		            	 st_config_trigger_t.threshold_volume+=1;
		             if (buffer_bin[3] == 49)
		            	 st_config_trigger_t.threshold_volume+=2;
		             if (buffer_bin[2] == 49)
		            	 st_config_trigger_t.threshold_volume+=4;
		             if (buffer_bin[1] == 49)
		            	 st_config_trigger_t.threshold_volume+=8;


		            st_config_trigger_t.max_temperature = OnehextoOneDec (temp_max);
		            st_config_trigger_t.min_temperature = OnehextoOneDec (temp_min);
		            if(st_config_trigger_t.min_temperature !=0)
		            	st_config_trigger_t.min_temperature -= 6;

		            st_config_trigger_t.max_volume =OnehextoOneDec (vol_max);
		            st_config_trigger_t.min_volume =OnehextoOneDec (vol_min);

		            st_config_time_t.inside_wt =OnehextoOneDec (t_transm1);
		            st_config_time_t.outside_wt =OnehextoOneDec (t_transm2);

		            if(st_config_trigger_t.max_volume !=0)
		            	st_config_trigger_t.max_volume = 150+((st_config_trigger_t.max_volume-1)*10);
		            if(st_config_trigger_t.min_volume !=0)
		            	st_config_trigger_t.min_volume = 5*(st_config_trigger_t.min_volume+(st_config_trigger_t.min_volume-2));


		             switch (st_config_time_t.temperature_sensor_reading) {
						case 0:
							st_config_time_t.temperature_sensor_reading = 86500;
							break;
						case 1:
							st_config_time_t.temperature_sensor_reading = sec_to_1_min;
							break;
						case 2:
							st_config_time_t.temperature_sensor_reading = sec_to_2_min;
							break;
						case 3:
							st_config_time_t.temperature_sensor_reading = sec_to_5_min;
							break;
						case 4:
							st_config_time_t.temperature_sensor_reading = sec_to_10_min;
							break;
						case 5:
							st_config_time_t.temperature_sensor_reading = sec_to_15_min;
							break;
						case 6:
							st_config_time_t.temperature_sensor_reading = sec_to_25_min;
							break;
						case 7:
							st_config_time_t.temperature_sensor_reading = sec_to_30_min;
							break;
					}
		             switch (st_config_time_t.volume_sensor_reading) {
						case 0:
							st_config_time_t.volume_sensor_reading = 86500;
							break;
						case 1:
							st_config_time_t.volume_sensor_reading = sec_to_1_min;
							break;
						case 2:
							st_config_time_t.volume_sensor_reading = sec_to_2_min;
							break;
						case 3:
							st_config_time_t.volume_sensor_reading = sec_to_5_min;
							break;
						case 4:
							st_config_time_t.volume_sensor_reading = sec_to_10_min;
							break;
						case 5:
							st_config_time_t.volume_sensor_reading = sec_to_15_min;
							break;
						case 6:
							st_config_time_t.volume_sensor_reading = sec_to_25_min;
							break;
						case 7:
							st_config_time_t.volume_sensor_reading = sec_to_30_min;
							break;
					}
		             switch (st_config_time_t.ph_sensor_reading) {
						case 0:
							st_config_time_t.ph_sensor_reading = 86500;
							break;
						case 1:
							st_config_time_t.ph_sensor_reading = sec_to_1_min;
							break;
						case 2:
							st_config_time_t.ph_sensor_reading = sec_to_2_min;
							break;
						case 3:
							st_config_time_t.ph_sensor_reading = sec_to_5_min;
							break;
						case 4:
							st_config_time_t.ph_sensor_reading = sec_to_10_min;
							break;
						case 5:
							st_config_time_t.ph_sensor_reading = sec_to_15_min;
							break;
						case 6:
							st_config_time_t.ph_sensor_reading = sec_to_25_min;
							break;
						case 7:
							st_config_time_t.ph_sensor_reading = sec_to_30_min;
							break;
					}
		             switch (st_config_time_t.inside_wt) {
						case 0:
							st_config_time_t.inside_wt = 86500;
							break;
						case 1:
							st_config_time_t.inside_wt = sec_to_30_min;
							break;
						case 2:
							st_config_time_t.inside_wt =sec_to_1_hour ;
							break;
						case 3:
							st_config_time_t.inside_wt = sec_to_2_hour;
							break;
						case 4:
							st_config_time_t.inside_wt = sec_to_6_hour;
							break;
						case 5:
							st_config_time_t.inside_wt = sec_to_8_hour;
							break;
						case 6:
							st_config_time_t.inside_wt = sec_to_12_hour;
							break;
						case 7:
							st_config_time_t.inside_wt = sec_to_24_hour;
							break;
					}
		             switch (st_config_time_t.outside_wt) {
						case 0:
							st_config_time_t.outside_wt = 86500;
							break;
						case 1:
							st_config_time_t.outside_wt = sec_to_30_min;
							break;
						case 2:
							st_config_time_t.outside_wt = sec_to_1_hour;
							break;
						case 3:
							st_config_time_t.outside_wt = sec_to_2_hour;
							break;
						case 4:
							st_config_time_t.outside_wt = sec_to_6_hour;
							break;
						case 5:
							st_config_time_t.outside_wt = sec_to_8_hour;
							break;
						case 6:
							st_config_time_t.outside_wt = sec_to_12_hour;
							break;
						case 7:
							st_config_time_t.outside_wt = sec_to_24_hour;
							break;
					}
		             if(st_config_trigger_t.threshold_volume==13)
		            	 st_config_trigger_t.threshold_volume = 70;
		             if(st_config_trigger_t.threshold_volume==14)
		            	 st_config_trigger_t.threshold_volume = 80;
		             if(st_config_trigger_t.threshold_volume==15)
		            	 st_config_trigger_t.threshold_volume = 90;
		             else
			            	 st_config_trigger_t.threshold_volume *= 5;
		        }
		    }
}

void fn_send_start_frame()
{
	fn_init_uart();
	char start_machine_frame[15]={0};
	//char payload_values[4]={0};
	char ok[4]={0};

	start_machine_frame[0]=65;
	start_machine_frame[1]=84;
	start_machine_frame[2]=36;
	start_machine_frame[3]=83;
	start_machine_frame[4]=70;
	start_machine_frame[5]=61;
	start_machine_frame[6]=48;
	start_machine_frame[7]=50;

	decHex(st_data_sensor_e.bat_value);
	size_2();
	strcat(start_machine_frame,buff);

	decHex(st_data_sensor_e.tpe_value);
	size_2();
	strcat(start_machine_frame,buff);

	start_machine_frame[12] = 13;
	start_machine_frame[13] = 10;
	start_machine_frame[14] = 0;
	HAL_UART_Transmit(&huart5,(uint8_t*)start_machine_frame,15,200);
	HAL_UART_Receive(&huart5,(uint8_t*)ok,4,100);

}
