/*
 * encoder.h
 *
 *  Created on: 1 de ago de 2019
 *      Author: oscar
 */

#ifndef ENCODER_H_
#define ENCODER_H_

#include "main.h"

#include <math.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include <lsm303agr_reg.h>
#include <sigfox.h>
#include <sensors.h>

//**************ENCODER VARIABLES*******************************
char buff[7];
char binary[32];
unsigned char buffer[5];
long long decimal;
//*****************HEADER VARIABLES*********************************
unsigned int hea_value;

void RemoveSpaces(char source[]);
void size_2 ();
void size_4 ();
void size_6 ();
void make_hex_string_learning(unsigned n, char *s);
void insertionSort(float vetor[], int tamanhoVetor);
void fn_init_uart();
void find_between(const char *first, const char *last,char *buff,char *buff_return);
int hexDec(char *hex);
int OnehextoOneDec(char hex[]);
void decHex(int number);
void decToBinary(int number);
int num_hex_digits(unsigned n);
void fn_mount_values(char payload[24]);
void hexBin (char *hex, char *dec);
int fn_get_seconsForTimeStemp(int TS_Total);
int binaryToDec(int num);
int bin_to_dec(char *bin);
#endif /* ENCODER_H_ */
