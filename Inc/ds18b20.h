/*
 * ds18b20.h
 *
 *  Created on: 1 de ago de 2019
 *      Author: oscar
 */

#ifndef DS18B20_H_
#define DS18B20_H_

#include <main.h>

#include <math.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include <lsm303agr_reg.h>
#include <sigfox.h>
#include <sensors.h>
#include <encoder.h>


#define DS18B20_SET         HAL_GPIO_WritePin(TEMP_EXT_GPIO_Port,TEMP_EXT_Pin,1);
#define DS18B20_RESET       HAL_GPIO_WritePin(TEMP_EXT_GPIO_Port,TEMP_EXT_Pin,0);

#define READ_ROM                0x33                // Read ROM command
#define MATCH_ROM               0x55                // Match ROM command
#define SKIP_ROM                0xCC                // Skip ROM command
#define READ_SCR                0xBE                // read scratchpad command

#define CONV_T                  0x44                // convert temperature

#define WRITE_MEM               0x0F                // Write memory command
#define VERITY_MEM              0xAA                // Verify memory command
#define COPY_MEM                0x55                // Copy memory command
#define READ_MEM                0xF0                // Read memory command


//***************DS18B20**********************************************************//
bool temp_negative;
uint8_t scratch[9];// = {0x00};
uint8_t byte;       // 8-byte counter
uint16_t convertido;
float temp_ds18b20; //temperature sensor ds18b20 celsius



void gpio_set_new_output (void);
void gpio_set_new_input (void);
void gpio_set_input (void);
void gpio_set_output (void);
void ds18b20_reset (void);
void ds18b20_write (uint8_t data);
uint8_t ds18b20_read (void);
void fn_get_ds18b20(void);

#endif /* DS18B20_H_ */
